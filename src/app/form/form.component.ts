import { Component} from '@angular/core';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent  {
  task = '';

  toDoList = [
    {task: 'by milk'},
    {task: 'go to school'},
    {task: 'washing  dishes'},
  ];

  formIsEmpty() {
    return this.task === ''
  }

  addTask(event: Event) {
    event.preventDefault();
    this.toDoList.push({
      task: this.task
    })
  }

  deleteTask(i: number) {
    this.toDoList.splice(i, 1);
  }

  changeTask(index: number, newTask: string) {
    this.toDoList[index].task = newTask;
  }
}
