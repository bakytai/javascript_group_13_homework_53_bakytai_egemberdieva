import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.css']
})
export class TaskComponent {
  @Input() task = '';
  @Output() delete = new EventEmitter();
  @Output() taskChange = new EventEmitter<string>();
  input = false;

  editInput(event: Event) {
    const target = <HTMLInputElement>event.target;
    this.taskChange.emit(target.value);
  }

  deleteInput() {
    this.delete.emit();
  }

  focusInput() {
    this.input = !this.input;
  }

  inputAwayFocus() {
    this.focusInput()
  }

  getInputClass() {
   let className = '';
   if (this.input) {
     className = 'red';
   }
   return 'input ' + className
  }
}
